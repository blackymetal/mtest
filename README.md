# MTest

Merqueo Test

## Some Handy Links

[ProductsController](https://bitbucket.org/blackymetal/mtest/src/master/app/Controller/ProductsController.php)

[CsvComponent](https://bitbucket.org/blackymetal/mtest/src/master/app/Controller/Component/CsvComponent.php)

[Product Model](https://bitbucket.org/blackymetal/mtest/src/master/app/Model/Product.php)

[Component Test](https://bitbucket.org/blackymetal/mtest/src/master/app/Test/Case/Controller/Component/CsvComponentTest.php)

[Controller Test](https://bitbucket.org/blackymetal/mtest/src/master/app/Test/Case/Controller/ProductsControllerTest.php)

[Server Test](https://uip.aduanacol.com)

