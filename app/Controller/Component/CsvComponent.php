<?php
App::uses('Component', 'Controller');

class CsvComponent extends Component
{
	public $controller = null;

	/**
	* Startup component
	*
	* @param object $controller Instantiating controller
	* @access public
	*/
	public function startup(Controller $controller)
	{
			$this->controller = $controller;
	}

	/**
	* process method
	*
	* Reads file line by line and execute each instruction,
	* although this is a task that should be executed in a cron job.
	*
	* @param string $path, usually the /tmp path
	* @return bool true if the file was processed false otherwise
	*/
	public function process($path)
	{
		$fh = fopen($path, "rb");
		if (!$fh) {
			return false;
		}
		// Starting Point
		$location = 0;

		// Assuming that the file could be huge, instead of loading the entire file, we only load one line at a time
		while (!feof($fh)) {
			fseek($fh, $location);
			$content = '';
			$char = '';

			// Lets find the \r or \ni (CRLF)
			while (!feof($fh)) {
				// Reads one char
				$char = fgetc($fh);
				$content .= $char;
				if ($char == "\r" || $char == "\n") {
					$content .= fgetc($fh);
					break;
				}
			}

			$length = strlen($content);
			$row = $content;

			// Updates the new location
			$location += $length;
			$csv = str_getcsv($row);
			$this->executeCommand($csv);
		}

		return true;
	}

	/**
	* executeCommand method
	*
	* Receives an array with one command, validates if the params for each command are complete and executes,
	* if the params are not complete returns false
	*
	* @param array $command, Ex: array(1, 'Agregar', 2), The first is the reference, second is the action the third
	* (optional per instruction) an amount
	* @return bool true if the command was processed false otherwise
	*/
	private function executeCommand($command)
	{
		if (count($command) < 2) {
			return false;
		}

		$reference = null;
		$action = null;
		$amount = null;

		$reference = $command[0];
		$action = strtolower($command[1]);

		if (!in_array($action, array('activar', 'desactivar', 'agregar', 'restar'))) {
			return false;
		}

		if (count($command) == 3) {
			$amount = trim($command[2]);
			if (!in_array($action, array('agregar', 'restar'))) {
				return false;
			}
		}

		switch ($action) {
			case 'agregar':
				return $this->controller->Product->updateAll(
					array('Product.units' => 'Product.units + ' . $amount),
					array('Product.reference' => $reference)
				);
				break;
			case 'restar':
				return $this->controller->Product->updateAll(
					array('Product.units' => 'Product.units - ' . $amount),
					array('Product.reference' => $reference)
				);
				break;
			case 'activar':
				return $this->controller->Product->updateAll(
					array('Product.status' => 1),
					array('Product.reference' => $reference)
				);
				break;
			case 'desactivar':
				return $this->controller->Product->updateAll(
					array('Product.status' => 0),
					array('Product.reference' => $reference)
				);
				break;
		}
		return true;
	}
}
