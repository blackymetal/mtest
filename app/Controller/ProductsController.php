<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 * @property CsvComponent $Csv
 */
class ProductsController extends AppController
{

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash', 'Csv');

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());
		$grouped = $this->Product->find(
			'list',
			array('conditions' => array('Product.parent_id' => null))
		);
		$this->set('grouped', $grouped);
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Product->create();
			$unique = $this->Product->checkUnique($this->request->data);
			if ($unique && $this->Product->save($this->request->data)) {
				$this->Flash->success(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else if ($unique) {
				$this->Flash->error(__('The product could not be saved. Please, try again.'));
			}
			if (!$unique) {
				$this->Flash->error(__('The product could not be saved. The Reference already exists. Please, change it.'));
			}
		}
		$grouped = $this->Product->find(
			'list',
			array('conditions' => array('Product.parent_id' => null))
		);
		$this->set('grouped', $grouped);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$unique = $this->Product->checkUnique($this->request->data);
			if ($unique && $this->Product->save($this->request->data)) {
				$this->Flash->success(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} elseif ($unique) {
				$this->Flash->error(__('The product could not be saved. Please, try again.'));
			}
			if (!$unique) {
				$this->Flash->error(__('The product could not be saved. The Reference already exists. Please, change it.'));
			}
		} else {
			$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
			$this->request->data = $this->Product->find('first', $options);
		}
		$grouped = $this->Product->find(
			'list',
			array('conditions' => array('Product.parent_id' => null))
		);
		$this->set('grouped', $grouped);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Product->delete()) {
			$this->Flash->success(__('The product has been deleted.'));
		} else {
			$this->Flash->error(__('The product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function upload()
	{
		if ($this->request->is('post')) {
			if (isset($this->request->data['Product']['csv_file']) && $this->request->data['Product']['csv_file']['error'] == 0) {
				if ($this->Csv->process($this->request->data['Product']['csv_file']['tmp_name'])) {
					$this->Flash->success(__('The CSV file has been uploaded.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('Something bad happened, please check the file and try again.'));
				}
			}
		}
	}
}
