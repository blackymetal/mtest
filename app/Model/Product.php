<?php
App::uses('AppModel', 'Model');
/**
 * Product Model
 *
 */
class Product extends AppModel
{

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	// public $actsAs = array('Tree');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'The Name is required'
			)
		),
		'reference' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'The Reference is required'
			)
		),
		'price' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'The Price is required'
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'The Price should contain a number'
			)
		),
		'cost' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'The Cost is required'
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'The Cost should contain a number'
			)
		),
		'units' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'The Units should contain a number'
			)
		),
		'status' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				'message' => 'The Status should be a boolean'
			)
		)
	);

	public function checkUnique($data)
	{
		$conditions = array();
		$conditions[] = array(
			'Product.reference' => $data['Product']['reference']
		);
		if (isset($data['Product']['id'])) {
			$conditions[] = array('NOT' => array('Product.id' => $data['Product']['id']));
		}
		$result = $this->find(
			'first',
			array('conditions' => $conditions)
		);
		return $result ? false : true;
	}
}
