<?php
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('CsvComponent', 'Controller/Component');
App::uses('ProductsController', 'Controller');

// A fake controller to test against
class ProductsControllerTest extends ProductsController
{
}

/**
 * CsvComponent Test Case
 */
class CsvComponentTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$Collection = new ComponentCollection();
		$this->Csv = new CsvComponent($Collection);
		$CakeRequest = new CakeRequest();
		$CakeResponse = new CakeResponse();
		$this->Controller = new ProductsControllerTest($CakeRequest, $CakeResponse);
		$this->Product = ClassRegistry::init('Product');
		$this->Controller->Product = $this->Product;
		$this->Csv->startup($this->Controller);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Csv);

		parent::tearDown();
	}

/**
 * testProcess method
 *
 * @return void
 */
	public function testProcess()
	{
		$path = WWW_ROOT . 'files' . DS . 'test.csv';
		$this->assertTrue($this->Csv->process($path));
		$result = $this->Product->find('first', array('conditions' => array('Product.reference' => 1)));
		$this->assertEqual($result['Product']['units'], 4);
	}
}
