<?php
App::uses('ProductsController', 'Controller');

/**
 * ProductsController Test Case
 */
class ProductsControllerTest extends ControllerTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product'
	);

	// Product Model Object
	public $Product = null;

	public function setUp()
	{
		parent::setUp();
		$this->Product = ClassRegistry::init('Product');
	}

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex()
	{
		$this->testAction('/products/', array(
			'method' => 'GET',
			'return' => 'contents'
		));
		$this->assertRegExp('/<html/', $this->contents);
		$this->assertRegExp('/<form/', $this->view);
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd()
	{
		$data = array(
				'Product' => array(
						'name' => 'My Name is...',
						'reference' => 1000,
						'status' => 0,
						'price' => '500',
						'cost' => '300',
						'units' => '0'
				)
		);
		$this->testAction(
			'/products/add',
			array('data' => $data, 'method' => 'post')
		);
		$product = $this->Product->find(
			'first',
			array('conditions' => array('Product.reference' => 1000))
		);

		$this->assertEquals($data['Product']['name'], $product['Product']['name']);
		$this->assertEquals($data['Product']['price'], $product['Product']['price']);
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit()
	{
		$data = array(
				'Product' => array(
						'id' => '1',
						'name' => 'I have changed',
						'reference' => 1,
						'status' => 0,
						'price' => '500',
						'cost' => '300',
						'units' => '0'
				)
		);
		$this->testAction(
			'/products/edit/1',
			array('data' => $data, 'method' => 'post')
		);
		$product = $this->Product->find(
			'first',
			array('conditions' => array('Product.reference' => $data['Product']['id']))
		);

		$this->assertEquals($data['Product']['id'], $product['Product']['id']);
		$this->assertEquals($data['Product']['name'], $product['Product']['name']);
		$this->assertEquals($data['Product']['price'], $product['Product']['price']);
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete()
	{
		$this->testAction(
			'/products/delete/1',
			array('method' => 'post')
		);
		$product = $this->Product->find(
			'first',
			array('conditions' => array('Product.reference' => 1))
		);
		$this->assertEmpty($product);
	}
}
