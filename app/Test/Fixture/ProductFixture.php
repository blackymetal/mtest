<?php
/**
 * Product Fixture
 */
class ProductFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'reference' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => true, 'key' => 'unique'),
		'price' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2', 'unsigned' => false),
		'cost' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2', 'unsigned' => false),
		'units' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'status' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'reference' => array('column' => 'reference', 'unique' => 1),
			'lft' => array('column' => 'lft', 'unique' => 0),
			'rght' => array('column' => 'rght', 'unique' => 0),
			'parent_id' => array('column' => 'parent_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '1',
			'name' => 'Avena Natural',
			'reference' => '1',
			'price' => '5850.00',
			'cost' => '4500.00',
			'units' => '1',
			'status' => 1,
			'parent_id' => null,
			'lft' => null,
			'rght' => null
		),
		array(
			'id' => '2',
			'name' => 'Queso Esparcible',
			'reference' => '2',
			'price' => '5100.00',
			'cost' => '3800.00',
			'units' => '1',
			'status' => 1,
			'parent_id' => null,
			'lft' => null,
			'rght' => null
		),
		array(
			'id' => '3',
			'name' => 'Test 1',
			'reference' => '3',
			'price' => '100.00',
			'cost' => '20.00',
			'units' => '1',
			'status' => 0,
			'parent_id' => null,
			'lft' => null,
			'rght' => null
		),
	);
}
