<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				MTest
			</a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li>
					<?php
					print $this->Html->link(
						'Products',
						array(
							'controller' => 'products',
							'action' => 'index'
						)
					);
					?>
				</li>
				<li>
					<?php
					print $this->Html->link(
						'Upload CSV File',
						array(
							'controller' => 'products',
							'action' => 'upload'
						)
					);
					?>
				</li>
			</ul>
		</div>
	</div>
</nav>
