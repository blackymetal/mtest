<div class="products form">
		<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Edit Product');; ?></h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?php
			echo $this->Form->create('Product', array('role' => 'form'));
			echo '<div class="form-group">';
			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Product \'s name'));
			echo '</div>';
			echo $this->Form->input('reference', array('class' => 'form-control', 'placeholder' => 'Barcode'));
			echo $this->Form->input('price', array('class' => 'form-control', 'placeholder' => 'Ej: 10000,00'));
			echo $this->Form->input('cost', array('class' => 'form-control', 'placeholder' => 'Ej: 10000,00'));
			echo $this->Form->input('units', array('class' => 'form-control', 'placeholder' => 'Ej: 20'));
			echo $this->Form->input(
				'parent_id',
				array(
					'class' => 'form-control',
					'placeholder' => 'Belongs to a group',
					'label' => 'Grouped',
					'options' => $grouped,
					'empty' => true
				)
			);
			echo $this->Form->input('status', array('class' => 'form-control', 'label' => 'Active'));
			?>
			<div class="form-group">
				<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-default')); ?>
			</div>
		</div>
	</div>
</div>
