<div class="products index">
	<h2><?php echo __('Products'); ?></h2>
	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i>'.__('New Product'), array('action' => 'add'), array('class' => 'btn', 'escape' => false)); ?>
	</div>
	<br/>
	<table class='table table-striped table-bordered'>
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('reference'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('cost'); ?></th>
			<th><?php echo $this->Paginator->sort('units'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('parent_id', 'Grouped'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($products as $product): ?>
	<tr>
		<td><?php echo h($product['Product']['id']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['name']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['reference']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['price']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['cost']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['units']); ?>&nbsp;</td>
		<td><?php echo ($product['Product']['status'] ? 'Active' : 'Disabled'); ?>&nbsp;</td>
		<td><?php echo ($product['Product']['parent_id'] ? $grouped[$product['Product']['parent_id']] : ''); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $product['Product']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $product['Product']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<?php
	print $this->element('pagination');
	?>
</div>
