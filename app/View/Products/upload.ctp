<div class="products form">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Upload CSV File'); ?></h1>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?php
			echo $this->Form->create('Product', array('role' => 'form', 'type' => 'file'));
			echo $this->Form->input(
				'csv_file',
				array(
					'between' => '<br />',
					'type' => 'file'
				)
			);
			?>
			<div class="form-group">
				<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-default'));?>
			</div>
		</div>
	</div>
</div>
